<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Language strings for "Upload and attribute a file (Xpert)" repository plugin
 * @package    repository_xpert_upload
 * @copyright  2013 University of Nottingham
 * @author     Barry Oosthuizen <barry.oosthuizen@nottingham.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['allrightsreserved'] = 'All rights reserved';
$string['author'] = 'Copyright holder';
$string['cc'] = 'Creative Commons';
$string['cc-nd'] = 'Creative Commons - NoDerivs';
$string['cc-nc-nd'] = 'Creative Commons - No Commercial NoDerivs';
$string['cc-nc'] = 'Creative Commons - No Commercial';
$string['cc-nc-sa'] = 'Creative Commons - No Commercial ShareAlike';
$string['cc-sa'] = 'Creative Commons - ShareAlike';
$string['chooselicense'] = 'Choose license';
$string['colour'] = 'Background and text colours';
$string['colourcombinationnotexists'] = 'The submitted colour combination does not exist';
$string['colourwhiteblack'] = 'Background: White, Text: Black';
$string['colourblackwhite'] = 'Background: Black, Text: White';
$string['colourgreyblack'] = 'Background: Light grey, Text: Black';
$string['colourbluebrown'] = 'Background: Baby blue, Text: Dark brown';
$string['colourpeachbrown'] = 'Background: Peach, Text: Dark brown';
$string['colouryellowblack'] = 'Background: Light yellow, Text: Black';
$string['colourpinkblack'] = 'Background: Light pink, Text: Black';
$string['configplugin'] = 'Configuration for upload plugin';
$string['debug'] = 'Debug images?';
$string['debug_description'] = 'Turning debug images on will cause images to be replaced by a blue image with debug info';
$string['invalidurl'] = 'Invalid URL specified.';
$string['invalidyear'] = 'Invalid copyright year specified.';
$string['large'] = 'Large: 1024 x 768 Max';
$string['medium'] = 'Medium: 500 x 375 Max';
$string['noauthorspecified'] = 'Error: No copyright holder specified.';
$string['pluginname_help'] = 'Upload and attribute a file to Moodle (Xpert)';
$string['pluginname'] = 'Upload and attribute a file (Xpert)';
$string['privacy:metadata'] = 'The Xpert upload and attribute a file repository does not store or transmit any personal information.';
$string['public'] = 'Public domain$string';
$string['small'] = 'Small: 240 x 180 Max';
$string['size'] = 'Image size';
$string['standard'] = 'Standard: 500 x 375 Max';
$string['upload:view'] = 'Use uploading in file picker';
$string['upload_error_ini_size'] = 'The uploaded file exceeds the upload_max_filesize directive in php.ini.';
$string['upload_error_form_size'] = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form.';
$string['upload_error_partial'] = 'The uploaded file was only partially uploaded.';
$string['upload_error_no_file'] = 'No file was uploaded.';
$string['upload_error_no_tmp_dir'] = 'PHP is missing a temporary folder.';
$string['upload_error_cant_write'] = 'Failed to write file to disk.';
$string['upload_error_extension'] = 'A PHP extension stopped the file upload.';
$string['url'] = 'URL';
$string['year'] = 'Year';
$string['unknown'] = 'Other';
$string['xpert_upload:view'] = 'Use the Xpert upload repository in file picker';
$string['xpert_upload:debug'] = 'Debug the Xpert upload repository';
