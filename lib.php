<?php
// This file is part of the Xpert upload repository plugin for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * A repository plugin to allow user uploading and copyright attribution of images
 *
 * @package    repository_xpert_upload
 * @copyright  2013 University of Nottingham
 * @author     Barry Oosthuizen <barry.oosthuizen@nottingham.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class repository_xpert_upload extends repository {
    /**
     * List of mimetypes
     * @var array||string
     */
    private $mimetypes;

    /**
     * Print a upload form
     * @return array
     */
    public function print_login() {
        return $this->get_listing();
    }

    /**
     * Process uploaded file
     * @param string $saveasfilename
     * @param int $maxbytes
     * @return \stdClass object containing details of the file uploaded
     */
    public function upload($saveasfilename, $maxbytes) {
        global $CFG;

        $types = optional_param_array('accepted_types', '*', PARAM_RAW);
        $savepath = optional_param('savepath', '/', PARAM_PATH);
        $itemid = optional_param('itemid', 0, PARAM_INT);
        $license = optional_param('license', $CFG->sitedefaultlicense, PARAM_TEXT);
        $author = optional_param('author', '', PARAM_TEXT);
        $size = optional_param('size', '', PARAM_TEXT);
        $url = optional_param('url', '', PARAM_RAW); // We will use param_clean to throw an error when invalid URL is entered.
        $year = optional_param('year', '', PARAM_RAW); // We will use param_clean to throw an error when invalid year is entered.
        $colour = optional_param('colour', '', PARAM_TEXT);

        $areamaxbytes = optional_param('areamaxbytes', FILE_AREA_MAX_BYTES_UNLIMITED, PARAM_INT);
        $overwriteexisting = optional_param('overwrite', false, PARAM_BOOL);

        return $this->process_upload($saveasfilename, $maxbytes, $types, $savepath, $itemid, $license, $author,
                $overwriteexisting, $areamaxbytes, $size, $url, $year, $colour);
    }

    /**
     * Do the actual processing of the uploaded file
     * @param string $saveasfilename name to give to the file
     * @param int $maxbytes maximum file size
     * @param mixed $types optional array of file extensions that are allowed or '*' for all
     * @param string $savepath optional path to save the file to
     * @param int $itemid optional the ID for this item within the file area
     * @param string $license optional the license to use for this file
     * @param string $author optional the name of the author of this file
     * @param bool $overwriteexisting optional user has asked to overwrite the existing file
     * @param int $areamaxbytes maximum size of the file area.
     * @param string $size
     * @param string $url
     * @param string $year
     * @param string $colour
     * @return \stdClass object containing details of the file uploaded
     * @throws moodle_exception
     * @throws file_exception
     */
    public function process_upload($saveasfilename, $maxbytes, $types = '*', $savepath = '/', $itemid = 0, $license = null,
            $author = '', $overwriteexisting = false, $areamaxbytes = FILE_AREA_MAX_BYTES_UNLIMITED, $size = 'standard',
            $url = '', $year = '', $colour = '') {

        global $USER, $CFG;

        if ((is_array($types) && in_array('*', $types)) || $types == '*') {
            $this->mimetypes = '*';
        } else {
            foreach ($types as $type) {
                $this->mimetypes[] = mimeinfo('type', $type);
            }
        }

        if ($license == null) {
            $license = $CFG->sitedefaultlicense;
        }

        $record = new stdClass();
        $record->filearea = 'draft';
        $record->component = 'user';
        $record->filepath = $savepath;
        $record->itemid = $itemid;
        $record->license = $license;
        $record->author = $author;

        $context = context_user::instance($USER->id);
        $elname = 'repo_upload_file';

        $fs = get_file_storage();

        if ($record->filepath !== '/') {
            $record->filepath = file_correct_filepath($record->filepath);
        }

        if (!isset($_FILES[$elname])) {
            throw new moodle_exception('nofile');
        }
        if (!empty($_FILES[$elname]['error'])) {
            switch ($_FILES[$elname]['error']) {
                case UPLOAD_ERR_INI_SIZE:
                    throw new moodle_exception('upload_error_ini_size', 'repository_xpert_upload');
                    break;
                case UPLOAD_ERR_FORM_SIZE:
                    throw new moodle_exception('upload_error_form_size', 'repository_xpert_upload');
                    break;
                case UPLOAD_ERR_PARTIAL:
                    throw new moodle_exception('upload_error_partial', 'repository_xpert_upload');
                    break;
                case UPLOAD_ERR_NO_FILE:
                    throw new moodle_exception('upload_error_no_file', 'repository_xpert_upload');
                    break;
                case UPLOAD_ERR_NO_TMP_DIR:
                    throw new moodle_exception('upload_error_no_tmp_dir', 'repository_xpert_upload');
                    break;
                case UPLOAD_ERR_CANT_WRITE:
                    throw new moodle_exception('upload_error_cant_write', 'repository_xpert_upload');
                    break;
                case UPLOAD_ERR_EXTENSION:
                    throw new moodle_exception('upload_error_extension', 'repository_xpert_upload');
                    break;
                default:
                    throw new moodle_exception('nofile');
            }
        }
        \core\antivirus\manager::scan_file($_FILES[$elname]['tmp_name'], $_FILES[$elname]['name'], true);

        $sourcefield = $this->get_file_source_info($_FILES[$elname]['name']);
        $record->source = self::build_source_field($sourcefield);

        if (empty($saveasfilename)) {
            $record->filename = clean_param($_FILES[$elname]['name'], PARAM_FILE);
        } else {
            $ext = '';
            $match = array();
            $filename = clean_param($_FILES[$elname]['name'], PARAM_FILE);
            if (strpos($filename, '.') === false) {
                // File has no extension at all - do not add a dot.
                $record->filename = $saveasfilename;
            } else {
                if (preg_match('/\.([a-z0-9]+)$/i', $filename, $match)) {
                    if (isset($match[1])) {
                        $ext = $match[1];
                    }
                }
                $ext = !empty($ext) ? $ext : '';
                if (preg_match('#\.(' . $ext . ')$#i', $saveasfilename)) {
                    // Saveas filename contains file extension already.
                    $record->filename = $saveasfilename;
                } else {
                    $record->filename = $saveasfilename . '.' . $ext;
                }
            }
        }

        // Check the file has some non-null contents - usually an indication that a user has tried to upload a folder by mistake.
        if (!$this->check_valid_contents($_FILES[$elname]['tmp_name'])) {
            throw new moodle_exception('upload_error_invalid_file', 'repository_xpert_upload', '', $record->filename);
        }

        if ($this->mimetypes != '*') {
            // Check filetype.
            $filemimetype = file_storage::mimetype($_FILES[$elname]['tmp_name'], $record->filename);
            if (!in_array($filemimetype, $this->mimetypes)) {
                throw new moodle_exception('invalidfiletype', 'repository', '',
                        get_mimetype_description(array('filename' => $_FILES[$elname]['name'])));
            }
        }

        if (empty($record->itemid)) {
            $record->itemid = 0;
        }

        if (($maxbytes !== -1) && (filesize($_FILES[$elname]['tmp_name']) > $maxbytes)) {
            throw new file_exception('maxbytes');
        }

        if (file_is_draft_area_limit_reached($record->itemid, $areamaxbytes, filesize($_FILES[$elname]['tmp_name']))) {
            throw new file_exception('maxareabytes');
        }

        $record->contextid = $context->id;
        $record->userid = $USER->id;
        $path = $_FILES[$elname]['tmp_name'];
        $attribute = new repository_xpert_upload_image($path, $author, $url, $license, $size, $year, $colour);
        $imagecontent = $attribute->get_image();

        if (repository::draftfile_exists($record->itemid, $record->filepath, $record->filename)) {
            $existingfilename = $record->filename;
            $unusedfilename = repository::get_unused_filename($record->itemid, $record->filepath, $record->filename);
            $record->filename = $unusedfilename;

            $storedfile = $fs->create_file_from_string($record, $imagecontent);
            if ($overwriteexisting) {
                repository::overwrite_existing_draftfile($record->itemid, $record->filepath, $existingfilename,
                        $record->filepath, $record->filename);

                $record->filename = $existingfilename;
            } else {
                $event = array();
                $event['event'] = 'fileexists';
                $event['newfile'] = new stdClass;
                $event['newfile']->filepath = $record->filepath;
                $event['newfile']->filename = $unusedfilename;
                $event['newfile']->url = moodle_url::make_draftfile_url($record->itemid, $record->filepath,
                        $unusedfilename)->out(false);

                $event['existingfile'] = new stdClass;
                $event['existingfile']->filepath = $record->filepath;
                $event['existingfile']->filename = $existingfilename;
                $event['existingfile']->url = moodle_url::make_draftfile_url($record->itemid, $record->filepath,
                        $existingfilename)->out(false);

                return $event;
            }
        } else {
            $storedfile = $fs->create_file_from_string($record, $imagecontent);
        }

        $filepath = moodle_url::make_draftfile_url($record->itemid, $record->filepath, $storedfile->get_filename())->out(false);

        return array(
            'url' => $filepath,
            'id' => $record->itemid,
            'file' => $record->filename);
    }

    /**
     * Checks the contents of the given file is not completely NULL - this can happen if a
     * user drags & drops a folder onto a filemanager / filepicker element
     * @param string $filepath full path (including filename) to file to check
     * @return true if file has at least one non-null byte within it
     */
    protected function check_valid_contents($filepath) {
        $buffersize = 4096;

        $fp = fopen($filepath, 'r');
        if (!$fp) {
            return false; // Cannot read the file - something has gone wrong.
        }
        while (!feof($fp)) {
            // Read the file 4k at a time.
            $data = fread($fp, $buffersize);
            if (preg_match('/[^\0]+/', $data)) {
                fclose($fp);
                return true; // Return as soon as a non-null byte is found.
            }
        }
        // Entire file is NULL.
        fclose($fp);
        return false;
    }

    /**
     * Return or echo out an upload form (depending on whether javascript is on or off)
     *
     * @param string $path
     * @param int $page
     * @return boolean||void
     */
    public function get_listing($path = '', $page = '') {
        $ret = array();
        $ret['list'] = array();
        $template = $this->get_upload_template_php();
        if ($this->options['ajax']) {

            $ret['nologin'] = true;
            $ret['nosearch'] = true;
            $ret['norefresh'] = true;
            $ret['dynload'] = false;
            $ret['upload'] = array('label' => get_string('attachment', 'repository'), 'id' => 'repo-form');
            $ret['allowcaching'] = true; // Indicates that result of get_listing() can be cached in filepicker.js.
        } else {
            echo $template;
        }
        return $ret;
    }

    /**
     * Get copyright license options
     *
     * @return array $licenses
     */
    public static function get_licenses() {
        $licenses = array();
        $licenses['unknown'] = get_string('unknown', 'repository_xpert_upload');
        $licenses['allrightsreserved'] = get_string('allrightsreserved', 'repository_xpert_upload');
        $licenses['public'] = get_string('public', 'repository_xpert_upload');
        $licenses['cc'] = get_string('cc', 'repository_xpert_upload');
        $licenses['cc-nd'] = get_string('cc-nd', 'repository_xpert_upload');
        $licenses['cc-nc-nd'] = get_string('cc-nc-nd', 'repository_xpert_upload');
        $licenses['cc-nc'] = get_string('cc-nc', 'repository_xpert_upload');
        $licenses['cc-nc-sa'] = get_string('cc-nc-sa', 'repository_xpert_upload');
        $licenses['cc-sa'] = get_string('cc-sa', 'repository_xpert_upload');

        return $licenses;
    }

    /**
     * Get image size options
     *
     * @return array $sizes
     */
    public static function get_sizes() {
        $sizes = array();
        $sizes['standard'] = get_string('standard', 'repository_xpert_upload');
        $sizes['small'] = get_string('small', 'repository_xpert_upload');
        $sizes['medium'] = get_string('medium', 'repository_xpert_upload');
        $sizes['large'] = get_string('large', 'repository_xpert_upload');

        return $sizes;
    }

    /**
     * Get the html for the upload form to be used when javascript is off
     *      *
     * @return string $template
     */
    public function get_upload_template_php() {
        $licenses = self::get_licenses();
        $sizes = self::get_sizes();

        $template = '
            <form method="post" enctype="multipart/form-data" style="display:inline">
            <table >
                <tr class="fp-file">
                    <td class="mdl-right"><label>' . get_string('attachment', 'repository') . '</label>:</td>
                    <td class="mdl-left"><input name="repo_upload_file" type="file"/></td></tr>
                <tr class="fp-saveas">
                    <td class="mdl-right"><label>' . get_string('saveas', 'repository') . '</label>:</td>
                    <td class="mdl-left"><input name="saveas" type="text"/></td></tr>
                <tr class="fp-setauthor">
                    <td class="mdl-right"><label>' . get_string('author', 'repository') . '</label>:</td>
                    <td class="mdl-left"><input name="author" type="text"/></td></tr>
                <tr class="fp-setlicense">
                    <td class="mdl-right"><label>' . get_string('chooselicense', 'repository') . '</label>:</td>
                    <td class="mdl-left">
                        <select id="license" name="license">';
        foreach ($licenses as $licensekey => $licensename) {
            $template    .= '<option value="' . $licensekey . '">' . $licensename . '</option>';
        }

        $template .= '</select>
                      </td></tr>
                  <tr class="size">
                      <td class="mdl-right"><label>' . get_string('size') . '</label>:</td>
                      <td class="mdl-left">
                          <select id="size" name="size">';
        foreach ($sizes as $sizekey => $sizename) {
            $template     .= '<option value="' . $sizekey . '">' . $sizename . '</option>';
        }

        $template     .= '</select>
                      </td></tr>
                  <tr class="fp-url">
                      <td class="mdl-right"><label>' . get_string('url', 'repository_xpert_upload') . '</label>:</td>
                      <td class="mdl-left"><input type="text" name="url"/></td></tr>
                  <tr class="fp-year">
                      <td class="mdl-right"><label>' . get_string('year', 'repository_xpert_upload') . '</label>:</td>
                      <td class="mdl-left"><input type="text" name="year"/></td></tr>
                  <tr class="fp-xpert_upload-colour">
                      <td class="mdl-right"><label>' . get_string('colour', 'repository_xpert_upload') . '</label>:</td>
                      <td class="mdl-left">
                      <div class="colourradiobutton">
                            <input type="radio" name="colour" value="colourwhiteblack" checked>
                            <div class="colourwhiteblack">' .
                                  get_string('colourwhiteblack', 'repository_xpert_upload')
                      . '</div></div><br/>
                      <div class="colourradiobutton">
                            <input type="radio" name="colour" value="colourblackwhite">
                            <div class="colourblackwhite">' .
                                  get_string('colourblackwhite', 'repository_xpert_upload')
                      . '</div></div><br/>
                      <div class="colourradiobutton">
                            <input type="radio" name="colour" value="colourgreyblack">
                            <div class="colourgreyblack">' .
                                  get_string('colourgreyblack', 'repository_xpert_upload')
                      . '</div></div><br/>
                      <div class="colourradiobutton">
                            <input type="radio" name="colour" value="colourbluebrown">
                            <div class="colourbluebrown">' .
                                  get_string('colourbluebrown', 'repository_xpert_upload')
                      . '</div></div><br/>
                      <div class="colourradiobutton">
                            <input type="radio" name="colour" value="colourpeachbrown">
                            <div class="colourpeachbrown">' .
                                  get_string('colourpeachbrown', 'repository_xpert_upload')
                      . '</div></div><br/>
                      <div class="colourradiobutton">
                            <input type="radio" name="colour value="colouryellowblack">
                            <div class="colouryellowblack">' .
                                  get_string('colouryellowblack', 'repository_xpert_upload')
                      . '</div></div><br/>
                      <div class="colourradiobutton">
                            <input type="radio" name="colour" value="colourpinkblack">
                            <div class="colourpinkblack">' .
                                  get_string('colourpinkblack', 'repository_xpert_upload')
                      . '</div></div><br/>
                      </td></tr>
              </table>
            <input name="action" value="upload" type="hidden"><br>
            <input name="draftpath" value="/" type="hidden"><br>
            <input name="savepath" value="/" type="hidden"><br>
            <input name="repo_id" value="' . $this->id . '" type="hidden"><br>
            <input value="Upload this file" type="submit" class="fp-login-submit btn-primary btn">
            </form>';
        return $template;
    }

    /**
     * Generate the template of the form
     * @global stdClass $CFG
     * @global moodle_page $PAGE
     * @return string The form template
     */
    public function get_upload_template() {
        $template = '
        <div class="fp-upload-form mdl-align">
            <div class="fp-content-center fp-formset">
                <form enctype="multipart/form-data" method="POST">
                    <table >
                        <tr class="fp-file control-group clearfix">
                            <td class="mdl-right">
                                <label class="control-label">' . get_string('attachment', 'repository') . '</label>:
                            </td>
                            <td class="mdl-left controls"><input type="file"/></td></tr>
                        <tr class="fp-saveas control-group clearfix">
                            <td class="mdl-right">
                                <label class="control-label">' . get_string('saveas', 'repository') . '</label>:
                            </td>
                            <td class="mdl-left controls"><input type="text"/></td></tr>
                        <tr class="fp-setauthor control-group clearfix">
                            <td class="mdl-right">
                                <label class="control-label">' . get_string('author', 'repository') . '</label>:
                            </td>
                            <td class="mdl-left controls"><input type="text"/></td></tr>
                        <tr class="fp-setlicense control-group clearfix">
                            <td class="mdl-right">
                                <label class="control-label">' . get_string('chooselicense', 'repository') . '</label>:
                            </td>
                            <td class="mdl-left controls"><select/></td></tr>
                        <tr class="fp-xpert_upload-size control-group clearfix">
                            <td class="mdl-right"><label class="control-label">' . get_string('size') . '</label>:</td>
                            <td class="mdl-left controls">
                                <select id="xpert_upload_size" name="size">
                                    <option value="standard">' . get_string('standard', 'repository_xpert_upload') . '</option>
                                    <option value="small">' . get_string('small', 'repository_xpert_upload') . '</option>
                                    <option value="medium">' . get_string('medium', 'repository_xpert_upload') . '</option>
                                    <option value="large">' . get_string('large', 'repository_xpert_upload') . '</option>
                                </select>
                            </td></tr>
                        <tr class="fp-url control-group clearfix">
                            <td class="mdl-right">
                                <label class="control-label">' . get_string('url', 'repository_xpert_upload') . '</label>:
                            </td>
                            <td class="mdl-left controls"><input type="text" name="url"/></td></tr>
                        <tr class="fp-year control-group clearfix">
                            <td class="mdl-right">
                                <label class="control-label">' . get_string('year', 'repository_xpert_upload') . '</label>:
                            </td>
                            <td class="mdl-left controls"><input type="text" name="year"/></td></tr>
                        <tr class="fp-xpert_upload-colour control-group clearfix">
                            <td class="mdl-right">
                                <label class="control-label">' . get_string('colour', 'repository_xpert_upload') . '</label>:
                            </td>
                            <td class="mdl-left controls">
                            <div class="colourradiobutton">
                                <input type="radio" name="colour" value="colourwhiteblack" checked>
                                <div class="colourwhiteblack">' .
                                        get_string('colourwhiteblack', 'repository_xpert_upload')
                            . '</div></div>
                            <div class="colourradiobutton">
                                <input type="radio" name="colour" value="colourblackwhite">
                                <div class="colourblackwhite">' .
                                        get_string('colourblackwhite', 'repository_xpert_upload')
                            . '</div></div>
                            <div class="colourradiobutton">
                            <input type="radio" name="colour" value="colourgreyblack">
                                <div class="colourgreyblack">' .
                                        get_string('colourgreyblack', 'repository_xpert_upload')
                            . '</div></div>
                            <div class="colourradiobutton">
                            <input type="radio" name="colour" value="colourbluebrown">
                                <div class="colourbluebrown">' .
                                        get_string('colourbluebrown', 'repository_xpert_upload')
                                . '</div></div>
                            <div class="colourradiobutton">
                                <input type="radio" name="colour" value="colourpeachbrown">
                                <div class="colourpeachbrown">' .
                                        get_string('colourpeachbrown', 'repository_xpert_upload')
                            . '</div></div>
                            <div class="colourradiobutton">
                                <input type="radio" name="colour" value="colouryellowblack">
                                <div class="colouryellowblack">' .
                                        get_string('colouryellowblack', 'repository_xpert_upload')
                            . '</div></div>
                            <div class="colourradiobutton">
                                <input type="radio" name="colour" value="colourpinkblack">
                                <div class="colourpinkblack">' .
                                        get_string('colourpinkblack', 'repository_xpert_upload')
                            . '</div></div>
                            </td></tr>
                    </table>
                </form>
                <div>
                    <button class="fp-upload-btn btn-primary btn">' . get_string('upload', 'repository') . '</button></div>
                </div>
        </div> ';

        return $template;
    }

    /**
     * supported return types
     * @return int
     */
    public function supported_returntypes() {
        return FILE_INTERNAL;
    }

    /**
     * Is this repository accessing private data?
     *
     * @return bool
     */
    public function contains_private_data() {
        return false;
    }

    /**
     * Type option names for the recorder
     *
     * @return array $options
     */
    public static function get_type_option_names() {
        return array('debug', 'pluginname');
    }

    /**
     * Edit/Create Admin Settings Moodle form
     *
     * @param moodleform $mform Moodle form (passed by reference)
     * @param string $classname repository class name
     */
    public static function type_config_form($mform, $classname = 'repository') {
        parent::type_config_form($mform, $classname);

        $mform->addElement('checkbox', 'debug', get_string('debug', 'repository_xpert_upload'));
        $mform->setType('debug', PARAM_INT);
        $mform->addElement('static', '', '', get_string('debug_description', 'repository_xpert_upload'));
        $mform->setDefault('debug', 0);
    }
}
